#Name: Sam LeCompte
#Date: 9/24/15
#Quiz2: p2

def whitePicture(picture):
  xmax = getWidth(picture)
  ymax = getHeight(picture)/2
  for x in range(0,xmax):
    for y in range(0,ymax):
      pixel = getPixelAt(picture,x,y)
      setColor(pixel, black)
  show(picture)
      
file = "/Users/slecompte2016/Documents/Week4/Quiz2/snowboard.jpg"
photo = makePicture(file)

whitePicture(photo)      