# Name: Sam LeCompte 
# Date: 9/22/15
# Project: P1

def whitePicture(picture):
  xmax = getWidth(picture)
  ymax = getHeight(picture)
  for x in range(0,xmax):
    for y in range(0,ymax):
      pixel = getPixelAt(picture,x,y)
      pBlue = getBlue(pixel)
      if(pBlue<150):
        setColor(pixel,white)
  
  show(picture)
      
file = "/Users/slecompte2016/Documents/Week4/Quiz2/snowboard.jpg"
photo = makePicture(file)

whitePicture(photo)