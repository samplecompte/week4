#Name: Sam LeCompte
#Date: 9/24/15
#Quiz2: p2

def whitePicture(picture):
  xmax = getWidth(picture)
  ymax = getHeight(picture)
  for x in range(0,xmax):
    for y in range(0,ymax/2):
      pixel = getPixelAt(picture,x,y)
      newY = (getHeight(picture)-1)-getY(pixel)
      target = getPixelAt(picture,x,newY)
      color = getColor(pixel)
      setColor(target,color)
  show(picture)
      
file = "/Users/slecompte2016/Documents/Week4/Quiz2/snowboard.jpg"
photo = makePicture(file)

whitePicture(photo)      