#Name: Sam LeCompte
#Date: 9/24/15
#Quiz2: p2

def whitePicture(picture):
  xmax = getWidth(picture)
  ymax = getHeight(picture)
  for x in range(0,xmax):
    for y in range(0,ymax):
      pixel = getPixelAt(picture,x,y)
      B = getBlue(pixel)+75
      R = getRed(pixel)+75
      G = getGreen(pixel)+75
      grey = (B + R + G)/3
      newColor = makeColor(grey, grey, grey)
      setColor(pixel, newColor)
  show(picture)
      
file = "/Users/slecompte2016/Documents/Week4/Quiz2/snowboard.jpg"
photo = makePicture(file)

whitePicture(photo)      