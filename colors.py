# Name: Sam LeCompte
# Date: 9/21/15 
# Color Project

setMediaPath("/Users/slecompte2016/Documents/Week4/frames")

def createFrames():
  for r in range(0,256):
    frame = makeEmptyPicture(256,256)
  
    for g in range(0, 256):
      for b in range(0, 256):
        pixel = getPixelAt(frame, g, b)
        color = makeColor(r,g,b)
        setColor(pixel, color)
  
    frameFile = "frame" + str(r) + ".png"
    writePictureTo(frame, frameFile)
  

#createFrames()    

movie = makeMovieFromInitialFile("frame0.png")
playMovie(movie) 